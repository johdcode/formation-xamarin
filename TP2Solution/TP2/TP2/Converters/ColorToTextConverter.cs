﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace TP2.Converters
{
    public class ColorToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color? color = value as Color?;

            if (color == null)
            {
                return "No Color";
            }

           return $"R={color.Value.R * 255:F0}, G={color.Value.G * 255:F0}, B= {color.Value.B * 255:F0}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
