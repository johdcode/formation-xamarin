﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3.Models
{
   public class Pizza
    {
        public string Nom { get; set; }
        public bool EstVegetarienne { get; set; }        
        public string Ingredients { get; set; }
        public double Prix { get; set; }
    }
}
