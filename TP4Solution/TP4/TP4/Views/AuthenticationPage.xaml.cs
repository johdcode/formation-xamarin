﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP4.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthenticationPage : ContentPage
    {
        public AuthenticationPage()
        {
            InitializeComponent();
        }

        private void AuthenticationViewModel_OnAuthenticateResult(object sender, bool authSuccess)
        {
            if (!authSuccess)
            {
                this.DisplayAlert("Authentification au service de banque", "Échec de l'authentification au service de banque.", "Fermer");
            }
            else
            {
                this.Navigation.PushAsync(new UserBankHomePage());
            }
        }
    }
}