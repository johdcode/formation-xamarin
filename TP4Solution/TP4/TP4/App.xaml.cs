﻿using System;
using TP4.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP4
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Views.AuthenticationPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
            Application.Current.Properties["OnSleep"] = DateTime.Now;
        }

        protected override void OnResume()
        {
            if(!Application.Current.Properties.TryGetValue("OnSleep", out object value))
            {
                return;
            }

            DateTime? onSleepDate = (DateTime?)value;

            if (onSleepDate == null)
            {
                return;
            }

            TimeSpan range = DateTime.Now - onSleepDate.Value;

            if (range.TotalSeconds > 10)
            {
                UserBankService.Instance.Logout();
            }
        }
    }
}
