﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TP4.Services;
using Xamarin.Forms;
using TP4.Extensions;
using System.Text.RegularExpressions;

namespace TP4.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        private Regex _ValidAccountNumber = new Regex("123[0-9]{7}");

        public event EventHandler<bool> OnAuthenticateResult;

        private string _AccountNumber;
        public string AccountNumber
        {
            get => this._AccountNumber;
            set
            {
                if (this.Set(value, ref this._AccountNumber))
                {
                    this.CmdAuthenticate.ChangeCanExecute();
                }
            }
        }

        private string _Password;
        public string Password
        {
            get => this._Password;
            set
            {
                if (this.Set(value, ref this._Password))
                {
                    this.CmdAddDigit.ChangeCanExecute();
                    this.CmdErasePassword.ChangeCanExecute();
                    this.CmdAuthenticate.ChangeCanExecute();
                }
            }
        }
        public Command CmdAuthenticate { get; set; }

        public Command<int> CmdAddDigit { get; set; }

        public Command CmdErasePassword { get; set; }

        public List<int> RandomDigits { get; set; }

        public AuthenticationViewModel()
        {
            this.CmdAuthenticate = new Command(this.Authenticate, this.CanAuthenticate);
            this.CmdAddDigit = new Command<int>(this.AddDigit, this.CanAddDigit);
            this.CmdErasePassword = new Command(this.ErasePassword, this.CanErasePassword);

            this.RandomDigits = Enumerable.Range(0, 10).RandomSort().ToList();
        }

        private void ErasePassword()
        {
            this.Password = null;
        }

        private bool CanErasePassword()
        {
            return this.Password != null;
        }

        private void Authenticate()
        {
            Debug.WriteLine("Account Number = {0}, Password = {1}", this.AccountNumber, this.Password);

            bool authSuccess = UserBankService.Instance.Authenticate(this.AccountNumber, this.Password);

            this.OnAuthenticateResult?.Invoke(this, authSuccess);

            if (authSuccess)
            {
                this.Password = null;
            }
        }

        private bool CanAuthenticate()
        {
            bool validAccountNumber = this._ValidAccountNumber.IsMatch(this.AccountNumber ?? string.Empty);
            bool validPassword = this.Password != null && this.Password.Length == 4;

            return validAccountNumber && validPassword;
        }

        private bool CanAddDigit(int digit)
        {
            return this.Password == null || this.Password.Length < 4;
        }

        private void AddDigit(int digit)
        {
            Debug.WriteLine($"New digit to add : {digit}");
            if (this.Password == null)
            {
                this.Password = string.Empty;
            }

            this.Password += digit;
        }
    }
}
