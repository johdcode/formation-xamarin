﻿using System;
using System.Collections.Generic;
using System.Text;
using TP4.Models;
using TP4.Services;
using Xamarin.Forms;

namespace TP4.ViewModels
{
    public class UserBankViewModel : BaseViewModel
    {
        private string _LastName;
        public string LastName
        {
            get => this._LastName;
            set
            {
                this.Set(value, ref this._LastName);
            }
        }

        public double Solde { get; set; } = 950;

        public Command CmdLogout { get; set; }

        public UserBankViewModel()
        {
            UserBank userBank = UserBankService.Instance.GetCurrentUser();

            if (userBank == null)
            {
                throw new Exception("There is no authenticated user ; We should'nt be here.");
            }

            this.LastName = userBank.LastName;

            this.CmdLogout = new Command(this.Logout);
        }

        private void Logout()
        {
            UserBankService.Instance.Logout();
        }
    }
}
