﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP4.Models
{
    public class UserBank
    {
        public int AccountNumber { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
