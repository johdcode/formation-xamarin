﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TP1_HomePage : ContentPage
    {
        public TP1_HomePage()
        {
            InitializeComponent();
        }

        private void imageButton_Clicked(object sender, EventArgs e)
        {
            this.Navigation.PushAsync(new TP1_TabbedPage());
        }
    }
}