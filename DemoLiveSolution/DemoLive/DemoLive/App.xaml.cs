﻿using DemoLive.Interfaces;
using DemoLive.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoLive
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            this.MainPage = new GeolocationPage();

            Debug.WriteLine("Current Idiom = {0}, Current Platform = {1}.", Device.Idiom, Device.RuntimePlatform);

            this.DemoSqlite();
        }

        private void DemoSqlite()
        {
            ISqlite sqlite = Xamarin.Forms.DependencyService.Get<ISqlite>();

            using (SQLite.SQLiteConnection connection = sqlite.GetConnection("demo.db"))
            {
                connection.CreateTable<Models.User>();

                connection.Insert(new Models.User { Name = "Tutu" });

                List<Models.User> users = connection.Table<Models.User>().ToList();
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
