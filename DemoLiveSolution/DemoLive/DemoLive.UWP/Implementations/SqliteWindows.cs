﻿using DemoLive.Interfaces;
using DemoLive.UWP.Implementations;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

[assembly:Dependency(typeof(SqliteWindows))]
namespace DemoLive.UWP.Implementations
{
    public class SqliteWindows : ISqlite
    {
        public SQLiteConnection GetConnection(string dbName)
        {
            string folderPath = Windows.Storage.ApplicationData.Current.LocalFolder.Path;

            string dbFullPath = Path.Combine(folderPath, dbName);

            return new SQLiteConnection(dbFullPath);
        }
    }
}
