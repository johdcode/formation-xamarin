﻿using DemoLive.Interfaces;
using DemoLive.UWP.Implementations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

[assembly: Xamarin.Forms.Dependency(typeof(ShowMessageWindows))]
namespace DemoLive.UWP.Implementations
{
    public class ShowMessageWindows : IShowMessage
    {
        public async void ShowMessage(string message)
        {
            Debug.WriteLine("Opening popup");

            await new MessageDialog(message).ShowAsync();

            // Bout de code ici va être appelé que lorsque la poup sera fermée
            Debug.WriteLine("Popup closed");
        }
    }
}
