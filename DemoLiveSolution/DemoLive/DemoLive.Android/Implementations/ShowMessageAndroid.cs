﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DemoLive.Droid.Implementations;
using DemoLive.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[assembly: Xamarin.Forms.Dependency(typeof(ShowMessageAndroid))]
namespace DemoLive.Droid.Implementations
{
    public class ShowMessageAndroid : IShowMessage
    {
        public void ShowMessage(string message)
        {
            Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }
    }
}